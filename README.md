
# libcurl-static rpm build

## purpose

a limited static lib with development files for building static c apps needing libcurl, build with openssl and c-ares

## provided files

```
%license COPYING
%{_libdir}/*.a
%{_includedir}/curl
%{_libdir}/*.so
```

## limited configuration:

```
LIBS="-ldl" %configure  --disable-shared --enable-static \
            --disable-ldap \
            --disable-sspi \
            --without-librtmp \
            --disable-ftp \
            --disable-dict \
            --disable-file \
            --disable-telnet \
            --disable-tftp \
            --disable-rtsp \
            --disable-pop3 \
            --disable-imap \
            --disable-smtp \
            --disable-gopher \
            --disable-smb \
            --without-libidn2 \
            --without-brotli \
            --without-libmetalink \
            --without-libpsl \
            --without-libssh
            --without-nss \
            --disable-symbol-hiding \
            --enable-ares \
            --with-ssl \
            --with-ca-bundle=%{_sysconfdir}/pki/tls/certs/ca-bundle.crt \
            --disable-unix-sockets \
            --disable-manual
```