#!/bin/bash
#
#  trap_print
#
# Created by Danny Goossen on 22/03/2018.
#
# MIT License
#
# Copyright (c) 2018 oc-runner, Gioxa Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
VERBOSE_COMMAND=1

function PreCommand() {
    exit_code=$?
    if   [ ! -z "${BASH_COMMAND%trap *}" ] \
     &&  [ "$VERBOSE_COMMAND" = "1" ] \
     && [ ! -z "${BASH_COMMAND%f_verbose_command*}" ] \
     && [ ! -z "${BASH_COMMAND%f_quiet_command*}" ] \
     && [ ! -z "${BASH_COMMAND%/bin/false*}" ] \
     && [ ! -z "${BASH_COMMAND%f_exit*}" ] \
     && [ ! -z "${BASH_COMMAND%f_fail*}" ] \
     &&  [ "$exit_code" = "0" ] \
     && ( [ ! -z "${BASH_COMMAND%echo*}" ] || [ -z "${BASH_COMMAND%echo* > *}" ] ) \
     && [ ! -z "${BASH_COMMAND%\[*}" ] \
     ; then
       IFS=' ' read -r -a t <<< "${BASH_COMMAND% > /dev/null}"
       y=""
       x=""
       element=""
       for element in "${t[@]}"
         do
           set +e
             eval "x=$(/bin/echo -e -n $element)" > /dev/null 2>&1
             exit_eval=$?
           set -e
           if [ $exit_eval -eq 0 ] && [ $element != "2>&1" ]; then
             y="$y $x"
           else
             y="$y $element"
           fi
         done

      if [ ! -z "${BASH_COMMAND%%*/dev/null*}" ]; then

         echo -e "\n\033[33m\$>$y\033[0;m"
      else

        if [ "${BASH_COMMAND%%>*/dev/null 2>&1}" = "$BASH_COMMAND" ]; then
          if [ "${BASH_COMMAND#*2>*}" = "$BASH_COMMAND" ]; then
            echo -e "\n\033[33m\$>$y \033[0;m \n   ( Silent, Suppressed \033[36mstdout\033[0;m )\n"
          else
            echo -e "\n\033[33m\$>${y%%2>*} \033[0;m \n   ( Silent, Suppressed \033[35mstderr\033[0;m )\n"
          fi
        else
          echo -e "\n\033[33m\$>${y%%>*/dev/null 2>&1} \033[0;m \n   ( Quiet, Suppressed \033[36mstdout\033[0;m & \033[35mstderr\033[0;m )\n"
        fi
      fi
    fi
    if [ "$exit_code" != "0" ]; then trap - DEBUG; fi
}

function f_exit() {
  exit_code=$?
  trap - EXIT
  trap - DEBUG

  if [ "$exit_code" != 0 ]; then
    echo -e "\033[31;1m --- FAILED $0 ---\033[0;m\n"
  else
    echo -e "\n\033[32;1m *** Success $0 ***\033[0;m\n"
  fi
 
  [ ! -z "$1" ] &&  $1 $exit_code
  exit $exit_code
}

set -e
set -o pipefail
function f_verbose_command() {
  VERBOSE_COMMAND=1
}
function f_quiet_command() {
  VERBOSE_COMMAND=0
}

function f_fail() {
  VERBOSE_COMMAND=0
  /bin/false
}

trap "f_exit" EXIT
trap "PreCommand" DEBUG
